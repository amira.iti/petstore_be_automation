*** Settings ***

Resource     ../common.robot

*** Variables ***


*** Keywords ***

 Edit Pet
    [Arguments]  ${pet_id}  ${pet_name}
     Create session    sessionname     ${BASE_URL}
     ${headers}      Set requests headers
     log  ${add_pet_data_jsn}
     ${data}=  get file  ${add_pet_data_jsn}
     ${data}=  Replace String  ${data}  {pet_id}    ${pet_id}
     ${data}=  Replace String  ${data}  {pet_name}    ${pet_name}
     ${resp}=  put request  sessionname	 /api/v3/pet  ${data}  headers=${headers}
     log  ${resp.text}
     [Return]   ${resp}   ${pet_id}    ${pet_name}
