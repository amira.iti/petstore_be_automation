*** Settings ***

Resource     ../common.robot

*** Variables ***


*** Keywords ***

 Delete Pet
    [Arguments]  ${pet_id}
     Create session    sessionname     ${BASE_URL}
     ${headers}   Set requests headers
     ${resp}=  DELETE request  sessionname	 /api/v3/pet/${pet_id}    headers=${headers}
     log  ${resp.text}
     [Return]   ${resp}
