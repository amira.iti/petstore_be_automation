*** Settings ***

Resource     ../common.robot

*** Variables ***


*** Keywords ***

 Generate pet random data
    [Arguments]          ${pet_name}=12           ${pet_id}=2
    ${pet_name}          Rondom Characters        ${pet_name}
    ${pet_id}            rondom numbers           ${pet_id}
    set test variable    ${pet_name}              ${pet_name}
    set test variable    ${pet_id}                ${pet_id}
    [Return]   ${pet_name}  ${pet_id}

 Add Pet
    [Arguments]  ${pet_id}  ${pet_name}     ${pet_status}
     Create session    sessionname     ${BASE_URL}
     ${headers}      Set requests headers
     log  ${add_pet_data_jsn}
     ${data}=  get file  ${add_pet_data_jsn}
     ${data}=  Replace String  ${data}  {pet_id}    ${pet_id}
     ${data}=  Replace String  ${data}  {pet_name}    ${pet_name}
     ${data}=  Replace String  ${data}  {pet_status}    ${pet_status}
     ${resp}=  post request  sessionname	 /api/v3/pet  ${data}  headers=${headers}
     log  ${resp.text}
     [Return]   ${resp}   ${pet_id}    ${pet_name}
