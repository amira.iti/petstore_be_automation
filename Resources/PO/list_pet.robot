*** Settings ***

Resource     ../common.robot

*** Variables ***


*** Keywords ***


 List Pet
    [Arguments]  ${pet_id}
     Create session    sessionname     ${BASE_URL}
     ${headers}   Set requests headers
     ${resp}=  get request  sessionname	     /api/v3/pet/${pet_id}    headers=${headers}
     log  ${resp.text}
     [Return]   ${resp}


 List Pet by status
     [Arguments]  ${status}
      Create session    sessionname     ${BASE_URL}
      ${headers}   Set requests headers
      ${resp}=  get request  sessionname	     /api/v3/pet/${status}    headers=${headers}
      log  ${resp.text}
      [Return]   ${resp}
