*** Settings ***

###############  Libraries ################
Library     String
Library		Collections
Library     RequestsLibrary
Library     FakerLibrary
Library     DateTime
Library	    JSONLibrary
Library     OperatingSystem


*** Variables ***
*** Variables ***

${username}     theUser
${password}     12345

############ URLS for Test Environment   #############

${BASE_URL}     https://petstore3.swagger.io


################################## Request Body paths ##############################


${add_pet_data_jsn}                               ${EXECDIR}/Request_Body/add_new_pet.json


*** Keywords ***



Login
     [Arguments]  ${usernaem}       ${password}
     Create session    sessionname     ${BASE_URL}
     ${resp}=  post request  sessionname	 /pi/v3/user/login?username=${usernaem}&password=${password}


Rondom Text
    [Arguments]  ${LENGTH}
      ${New_Random_text}  Generate Random String	${LENGTH}	[LETTERS]
    [Return]  ${New_Random_text}

Rondom Numbers
        [Arguments]  ${LENGTH}
        ${Random_numbers}  Generate Random String	${LENGTH}	[NUMBERS]
        ${Random_numbers}  get substring  ${Random_numbers}  1
        ${first_digit}  FakerLibrary.Random Digit Not Null
        ${New_Random_numbers}  catenate  ${first_digit}${Random_numbers}
        [Return]  ${New_Random_numbers}

Rondom Characters
    [Arguments]  ${LENGTH}
      ${New_Random_text}  Generate Random String	${LENGTH}	[NUMBERS][LETTERS]
    [Return]  ${New_Random_text}

Set requests headers
   ${headers}=  Create Dictionary  accept=application/xml  Content-Type=application/json
   log  ${headers}
   [Return]  ${headers}

Set requests headerss
     ${headers}=  Create Dictionary  accept=application/xml
     log  ${headers}
     [Return]  ${headers}


