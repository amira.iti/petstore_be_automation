#####  Running Commands  ########
# robot   -d    Results  -i       Happy              Tests
# robot   -d    Results  -i       edit_pet         Tests
# robot   -d    Results  -i       invalid         Tests
# robot   -d    Results  -i       petstore         Tests


*** Settings ***

Resource    ../Resources/common.robot
Resource    ../Resources/PO/edit_pet.robot
Resource    ../Resources/PO/add_new_pet.robot

*** Variables ***


*** Test Cases ***

Edit Pet
   [Tags]    petstore    Happy      edit_pet
   ${pet_name}  ${pet_id}    Generate pet random data
     login    ${username}     ${password}

   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}     pending
   log    ${pet_name}
   Should Be Equal As Strings    ${resp.status_code}   200
   ${updated_pet_name}   Rondom text    8
   ${resp}  ${pet_id}    ${pet_name}   Edit Pet   ${pet_id}  ${updated_pet_name}
   Should Be Equal As Strings    ${resp.status_code}   200

Edit Pet with invalid pet name
   [Tags]    petstore    Happy      edit_pet
   ${pet_name}  ${pet_id}    Generate pet random data
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}     pending
   log    ${pet_name}
   Should Be Equal As Strings    ${resp.status_code}   200
   ${updated_pet_name}   Rondom Characters    8
   ${resp}  ${pet_id}    ${pet_name}   Edit Pet   ${pet_id}  updated_${updated_pet_name}
   Should Be Equal As Strings    ${resp.status_code}   404

 Edit Pet with missing pet name
    [Tags]    petstore    Happy      edit_pet
    ${pet_name}  ${pet_id}    Generate pet random data
    login    ${username}     ${password}
    ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}        pending
    log    ${pet_name}
    Should Be Equal As Strings    ${resp.status_code}   200
    ${resp}  ${pet_id}    ${pet_name}   Edit Pet   ${pet_id}     ${EMPTY}
   Should Be Equal As Strings    ${resp.status_code}   404
