#####  Running Commands  ########
# robot   -d    Results  -i       add_new_pet         Tests
# robot   -d    Results  -i       Happy              Tests
# robot   -d    Results  -i       petstore           Tests
# robot   -d    Results  -i       invalid         Tests
# robot   -d    Results  -i       petstore         Tests


*** Settings ***

Resource    ../Resources/common.robot
Resource    ../Resources/PO/add_new_pet.robot




*** Test Cases ***

Add New Pet
   [Tags]    petstore    Happy      add_new_pet
  ${pet_name}  ${pet_id}    Generate pet random data
   login    ${username}     ${password}
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}     pending
   log    ${pet_name}
   Should Be Equal As Strings    ${resp.status_code}   200

Add New Pet with missing pet data
   [Tags]    petstore    Happy      add_new_pet   invalid
   ${pet_name}  ${pet_id}    Generate pet random data
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${EMPTY}  ${EMPTY}     pending
   log    ${pet_name}
   log   ${pet_id}
   Should Be Equal As Strings    ${resp.status_code}   400

Add New Pet with missing pet ID
   [Tags]    petstore    Happy      add_new_pet   invalid
   ${pet_name}  ${pet_id}    Generate pet random data
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${EMPTY}  ${pet_name}      pending
   log    ${pet_name}
   log   ${pet_id}
   Should Be Equal As Strings    ${resp.status_code}   400

#Only ID is mandatory
Add New Pet with missing pet name
   [Tags]    petstore    Happy      add_new_pet   invalid
   ${pet_name}  ${pet_id}    Generate pet random data
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}   ${EMPTY}       pending
   log    ${pet_name}
   log   ${pet_id}
   Should Be Equal As Strings    ${resp.status_code}   200

Add New Pet with invalid data
   [Tags]    petstore    Happy      add_new_pet   invalid
   ${pet_name}  ${pet_id}    Generate pet random data
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   %%%$$######    ${pet_name}     pending
   log    ${pet_name}
   log   ${pet_id}
   Should Be Equal As Strings    ${resp.status_code}   400