#####  Running Commands  ########
# robot   -d    Results  -i       Happy            Tests
# robot   -d    Results  -i       list_pet         Tests
# robot   -d    Results  -i       invalid          Tests
# robot   -d    Results  -i       petstore         Tests


*** Settings ***

Resource    ../Resources/common.robot
Resource    ../Resources/PO/add_new_pet.robot
Resource    ../Resources/PO/delete_pet.robot


*** Test Cases ***

Delete Pet
   [Tags]    petstore    Happy      delete_pet
   ${pet_name}  ${pet_id}    Generate pet random data
   login    ${username}     ${password}
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}     pending
   Should Be Equal As Strings    ${resp.status_code}   200
   ${resp}    Delete Pet   ${pet_id}
   Should Be Equal As Strings    ${resp.status_code}   200

#It should Fail
Delete Pet with ID not found
   [Tags]    petstore    Happy      delete_pet   invalid
   ${resp}    Delete Pet   66666667777
   Should Be Equal As Strings    ${resp.status_code}   400

Delete Pet with missing ID
   [Tags]    petstore    Happy      delete_pet   invalid
   ${resp}    Delete Pet   ${EMPTY}
   Should Be Equal As Strings    ${resp.status_code}   405
