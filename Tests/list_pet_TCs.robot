#####  Running Commands  ########
# robot   -d    Results  -i       Happy            Tests
# robot   -d    Results  -i       list_pet         Tests
# robot   -d    Results  -i       invalid          Tests
# robot   -d    Results  -i       petstore         Tests


*** Settings ***

Resource    ../Resources/common.robot
Resource    ../Resources/PO/add_new_pet.robot
Resource    ../Resources/PO/list_pet.robot


*** Variables ***



*** Test Cases ***

List Pet
   [Tags]    petstore   Happy      list_pet
   ${pet_name}  ${pet_id}    Generate pet random data
   login    ${username}     ${password}
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}     pending
   Should Be Equal As Strings    ${resp.status_code}   200
   ${resp}    List Pet   ${pet_id}
   Should Be Equal As Strings    ${resp.status_code}   200


List Pet with ID not found
   [Tags]    petstore    Happy      list_pet   invalid
   login    ${username}     ${password}
   ${resp}    List Pet   66666667777
   Should Be Equal As Strings    ${resp.status_code}   404


List Pet with missing ID
   [Tags]    petstore    Happy      list_pet   invalid
   login    ${username}     ${password}
   ${resp}    List Pet   ${EMPTY}
   Should Be Equal As Strings    ${resp.status_code}   405

 List all pending pet
    [Tags]    petstore    Happy    list_pet     list_pet_pending
    ${pet_name}  ${pet_id}    Generate pet random data
    login    ${username}     ${password}
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}     pending
    Should Be Equal As Strings    ${resp.status_code}   200
    ${resp}     List Pet by status    pending
    Should Be Equal As Strings    ${resp.status_code}   200

 List all available pet
    [Tags]    petstore    Happy   list_pet    list_pet_available
    ${pet_name}  ${pet_id}    Generate pet random data
    login    ${username}     ${password}
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}     pending
    Should Be Equal As Strings    ${resp.status_code}   200
    ${resp}     List Pet by status    available
    Should Be Equal As Strings    ${resp.status_code}   200

 List all sold pet
   [Tags]    petstore    Happy      list_pet     list_pet_sold
   ${pet_name}  ${pet_id}    Generate pet random data
   login    ${username}     ${password}
   Should Be Equal As Strings    ${resp.status_code}   200
   ${resp}     List Pet by status    sold
   Should Be Equal As Strings    ${resp.status_code}   200
