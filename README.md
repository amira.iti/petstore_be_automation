# Petsore BE Automation using Robot Framework 
# Why Robot Framework

- Open source
- Very easy to install
- RF is application and platform independent
- User doesn't need a programming language to write a Robot Framework test case & run it
- Supports Keyword driven, Data-driven and Behaviour-driven (BDD) approaches
- The use of Selenium2 library in RF will help  to automate the existing manual test cases of a project in a Rapid Phase
- Outstanding Reporting system: The RF automatically generates a report and html log after executing each build
- Robot Framework provides lots of libraries to test different application like Appium Library for Mobile Automation, Database Library for DB testing, Android Library etc.


## Key Feature List

- #### Add new Pet
- #### List  Pet
- #### List  Pet by different status
- #### Edit  Pet
- #### Delete Pet


## How to install Robot Framework with Python


- Robot Framework is implemented with Python, so you need to have Python installed.
On Windows machines, make sure to add Python to PATH during installation.

- Installing Robot Framework with pip is simple:

`pip install robotframework`

- To check that the installation was succesful, run

`robot --version`

- For a full guide, please see Installation instructions. It also covers topics such as running Robot Framework on Jython (JVM) and IronPython (.NET).

**Now you are ready to write your first tests!**

## Other Dependencies 

```
pip install robotframework
pip install robotframework-faker
pip install robotframework-SeleniumLibrary
pip install robotframework-jsonlibrary
pip install robotframework-requests
```


## Robot Framework Project structure 

- ###### Request Body folder that contains all json data for all requests
- ###### Tests folder that contains all test cases 
- ###### Resources folder that will include all common and all page object pages 
- ###### Results Folder that all have the result report along with the tags


## Robot Framework Test Cases 

- #### Add new Pet

```
*** Settings ***

Resource    ../Resources/common.robot
Resource    ../Resources/PO/add_new_pet.robot

*** Test Cases ***

Add New Pet
   [Tags]    petstore    Happy      add_new_pet
  ${pet_name}  ${pet_id}    Generate pet random data
   login    ${username}     ${password}
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}     pending
   log    ${pet_name}
   Should Be Equal As Strings    ${resp.status_code}   200

Add New Pet with missing pet data
   [Tags]    petstore    Happy      add_new_pet   invalid
   ${pet_name}  ${pet_id}    Generate pet random data
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${EMPTY}  ${EMPTY}     pending
   log    ${pet_name}
   log   ${pet_id}
   Should Be Equal As Strings    ${resp.status_code}   400

Add New Pet with missing pet ID
   [Tags]    petstore    Happy      add_new_pet   invalid
   ${pet_name}  ${pet_id}    Generate pet random data
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${EMPTY}  ${pet_name}      pending
   log    ${pet_name}
   log   ${pet_id}
   Should Be Equal As Strings    ${resp.status_code}   400

Add New Pet with missing pet name
   [Tags]    petstore    Happy      add_new_pet   invalid
   ${pet_name}  ${pet_id}    Generate pet random data
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}   ${EMPTY}       pending
   log    ${pet_name}
   log   ${pet_id}
   Should Be Equal As Strings    ${resp.status_code}   200

Add New Pet with invalid data
   [Tags]    petstore    Happy      add_new_pet   invalid
   ${pet_name}  ${pet_id}    Generate pet random data
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   %%%$$######    ${pet_name}     pending
   log    ${pet_name}
   log   ${pet_id}
   Should Be Equal As Strings    ${resp.status_code}   400
```


- #### List  Pet
- #### List  Pet by different status

```
*** Settings ***

Resource    ../Resources/common.robot
Resource    ../Resources/PO/add_new_pet.robot
Resource    ../Resources/PO/list_pet.robot

*** Variables ***


*** Test Cases ***

List Pet
   [Tags]    petstore   Happy      list_pet
   ${pet_name}  ${pet_id}    Generate pet random data
   login    ${username}     ${password}
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}     pending
   Should Be Equal As Strings    ${resp.status_code}   200
   ${resp}    List Pet   ${pet_id}
   Should Be Equal As Strings    ${resp.status_code}   200


List Pet with ID not found
   [Tags]    petstore    Happy      list_pet   invalid
   login    ${username}     ${password}
   ${resp}    List Pet   66666667777
   Should Be Equal As Strings    ${resp.status_code}   404


List Pet with missing ID
   [Tags]    petstore    Happy      list_pet   invalid
   login    ${username}     ${password}
   ${resp}    List Pet   ${EMPTY}
   Should Be Equal As Strings    ${resp.status_code}   405

 List all pending pet
    [Tags]    petstore    Happy    list_pet     list_pet_pending
    ${pet_name}  ${pet_id}    Generate pet random data
    login    ${username}     ${password}
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}     pending
    Should Be Equal As Strings    ${resp.status_code}   200
    ${resp}     List Pet by status    pending
    Should Be Equal As Strings    ${resp.status_code}   200

 List all available pet
    [Tags]    petstore    Happy   list_pet    list_pet_available
    ${pet_name}  ${pet_id}    Generate pet random data
    login    ${username}     ${password}
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}     pending
    Should Be Equal As Strings    ${resp.status_code}   200
    ${resp}     List Pet by status    available
    Should Be Equal As Strings    ${resp.status_code}   200

 List all sold pet
   [Tags]    petstore    Happy      list_pet     list_pet_sold
   ${pet_name}  ${pet_id}    Generate pet random data
   login    ${username}     ${password}
   Should Be Equal As Strings    ${resp.status_code}   200
   ${resp}     List Pet by status    sold
   Should Be Equal As Strings    ${resp.status_code}   200
```


- #### Edit  Pet


```
*** Settings ***

Resource    ../Resources/common.robot
Resource    ../Resources/PO/edit_pet.robot
Resource    ../Resources/PO/add_new_pet.robot

*** Variables ***


*** Test Cases ***

Edit Pet
   [Tags]    petstore    Happy      edit_pet
   ${pet_name}  ${pet_id}    Generate pet random data
     login    ${username}     ${password}

   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}     pending
   log    ${pet_name}
   Should Be Equal As Strings    ${resp.status_code}   200
   ${updated_pet_name}   Rondom text    8
   ${resp}  ${pet_id}    ${pet_name}   Edit Pet   ${pet_id}  ${updated_pet_name}
   Should Be Equal As Strings    ${resp.status_code}   200

Edit Pet with invalid pet name
   [Tags]    petstore    Happy      edit_pet
   ${pet_name}  ${pet_id}    Generate pet random data
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}     pending
   log    ${pet_name}
   Should Be Equal As Strings    ${resp.status_code}   200
   ${updated_pet_name}   Rondom Characters    8
   ${resp}  ${pet_id}    ${pet_name}   Edit Pet   ${pet_id}  updated_${updated_pet_name}
   Should Be Equal As Strings    ${resp.status_code}   404

 Edit Pet with missing pet name
    [Tags]    petstore    Happy      edit_pet
    ${pet_name}  ${pet_id}    Generate pet random data
    login    ${username}     ${password}
    ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}        pending
    log    ${pet_name}
    Should Be Equal As Strings    ${resp.status_code}   200
    ${resp}  ${pet_id}    ${pet_name}   Edit Pet   ${pet_id}     ${EMPTY}
   Should Be Equal As Strings    ${resp.status_code}   404
```

- #### Delete Pet

```
*** Settings ***

Resource    ../Resources/common.robot
Resource    ../Resources/PO/add_new_pet.robot
Resource    ../Resources/PO/delete_pet.robot


*** Test Cases ***

Delete Pet
   [Tags]    petstore    Happy      delete_pet
   ${pet_name}  ${pet_id}    Generate pet random data
   login    ${username}     ${password}
   ${resp}  ${pet_id}    ${pet_name}   Add Pet   ${pet_id}  ${pet_name}     pending
   Should Be Equal As Strings    ${resp.status_code}   200
   ${resp}    Delete Pet   ${pet_id}
   Should Be Equal As Strings    ${resp.status_code}   200

#It should Fail
Delete Pet with ID not found
   [Tags]    petstore    Happy      delete_pet   invalid
   ${resp}    Delete Pet   66666667777
   Should Be Equal As Strings    ${resp.status_code}   400

Delete Pet with missing ID
   [Tags]    petstore    Happy      delete_pet   invalid
   ${resp}    Delete Pet   ${EMPTY}
   Should Be Equal As Strings    ${resp.status_code}   405
```



## Run the Test

### To run all test cases 
` robot   -d    Results  -i       petstore         Tests`

### To run add pet test cases 
`robot   -d    Results  -i       add_pet         Tests`

### To run edit pet test cases
` robot   -d    Results  -i       edit_pet         Tests`

### To run list all pet test cases 
`robot   -d    Results  -i       list_pet         Tests`

### To run list by status  pet test cases
```
 robot   -d    Results  -i       list_pet_available         Tests
 robot   -d    Results  -i       list_pet_pending           Tests
 robot   -d    Results  -i       list_pet_sold             Tests
```


### To run delete pet test cases
`robot   -d    Results  -i       delete_pet       Tests`

### To run all happy scenarios test cases
`robot   -d    Results  -i       Happy            Tests`


### To run all invalid scenarios test cases
`robot   -d    Results  -i       invalid            Tests`
